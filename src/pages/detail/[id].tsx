import type { NextPage } from "next";
import React from "react";
import Head from "next/head";
import MovieDetail from "@/src/components/MovieDetail";

const Home: NextPage = () => {
  return (
    <>
      <Head>
        <title>Detail</title>
      </Head>
      <MovieDetail />
    </>
  );
};

export default Home;
