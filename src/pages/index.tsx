import type { NextPage } from "next";
import React from "react";
import Head from "next/head";
import MovieList from "../components/MovieLists";

const Home: NextPage = () => {
  return (
    <>
      <Head>
        <title>List</title>
      </Head>
      <MovieList />
    </>
  );
};

export default Home;
