export interface TrailerInterface {
  type: string;
  key: string;
}
export interface VisitInterface {
  children: any;
  message: string;
}

export interface DetailInterface {
  backdrop_path: string;
  poster_path: string;
}
