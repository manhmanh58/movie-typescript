import { API_KEY } from "constants/common";
import { axiosClient } from "../api/axiosClient";

export const getMovies = async (pageParam = 1) => {
  const response = await axiosClient.get(
    `popular?api_key=${API_KEY}&language=en-US&page=${pageParam}`
  );
  return response.data;
};

export const getMovieDetails = async (movieId: string) => {
  const response = await axiosClient.get(
    `${movieId}?api_key=${API_KEY}&language=en-US`
  );
  return response.data;
};

export const getTrailer = async (movieId: string) => {
  const response = await axiosClient.get(
    `${movieId}/videos?api_key=${API_KEY}&language=en-US`
  );
  return response.data;
};

export const getCredit = async (movieId: string) => {
  const response = await axiosClient.get(
    `${movieId}/credits?api_key=${API_KEY}&language=en-US`
  );
  return response.data;
};

export const getReview = async (movieId: string) => {
  const response = await axiosClient.get(
    `${movieId}/reviews?api_key=${API_KEY}&language=en-US`
  );
  return response.data;
};

export const getRecommendation = async (movieId: string) => {
  const response = await axiosClient.get(
    `${movieId}/recommendations?api_key=${API_KEY}&language=en-US`
  );
  return response.data;
};

export const getKeyword = async (movieId: string) => {
  const response = await axiosClient.get(
    `${movieId}/keywords?api_key=${API_KEY}&language=en-US`
  );
  return response.data;
};

export const getPoster = async (movieId: string) => {
  const response = await axiosClient.get(
    `${movieId}/images?api_key=${API_KEY}`
  );
  return response.data;
};
