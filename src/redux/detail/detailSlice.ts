import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import {
  getMovieDetails,
  getTrailer,
  getCredit,
  getReview,
  getRecommendation,
  getKeyword,
  getPoster,
} from "services/movieAPI";

const detailSlice = createSlice({
  name: "movieDetail",
  initialState: {
    detail: {
      backdrop_path: "",
      poster_path: "",
      vote_average: 0,
      title: "",
      release_date: "",
      genres: [{}],
      runtime: 0,
      tagline: "",
      overview: "",
    },
    trailer: [{ type: "", key: "" }],
    profile: { cast: [] },
    review: [],
    recommendations: [],
    keyword: [],
    images: { backdrops: [{ file_path: "" }], posters: [{ file_path: "" }] },
  },
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(fetchDetail.fulfilled, (state, action) => {
        state.detail = action.payload;
      })
      .addCase(fetchTrailer.fulfilled, (state, action) => {
        state.trailer = action.payload.results;
      })
      .addCase(fetchProfile.fulfilled, (state, action) => {
        state.profile = action.payload;
      })
      .addCase(fetchReview.fulfilled, (state, action) => {
        state.review = action.payload.results;
      })
      .addCase(fetchRecommendation.fulfilled, (state, action) => {
        state.recommendations = action.payload.results;
      })
      .addCase(fetchKeyword.fulfilled, (state, action) => {
        state.keyword = action.payload.keywords;
      })
      .addCase(fetchPoster.fulfilled, (state, action) => {
        state.images = action.payload;
      });
  },
});

export const fetchDetail = createAsyncThunk(
  "detail/fetchDetail",
  async (id: string) => {
    const res = await getMovieDetails(id);
    return res;
  }
);

export const fetchTrailer = createAsyncThunk(
  "detail/fetchTrailer",
  async (id: string) => {
    const res = await getTrailer(id);
    return res;
  }
);

export const fetchProfile = createAsyncThunk(
  "detail/fetchProfile",
  async (id: string) => {
    const res = await getCredit(id);
    return res;
  }
);

export const fetchReview = createAsyncThunk(
  "detail/fetchReview",
  async (id: string) => {
    const res = await getReview(id);
    return res;
  }
);

export const fetchRecommendation = createAsyncThunk(
  "detail/Recommendation",
  async (id: string) => {
    const res = await getRecommendation(id);
    return res;
  }
);

export const fetchKeyword = createAsyncThunk(
  "detail/Keyword",
  async (id: string) => {
    const res = await getKeyword(id);
    return res;
  }
);

export const fetchPoster = createAsyncThunk(
  "detail/Poster",
  async (id: string) => {
    const res = await getPoster(id);
    return res;
  }
);

export default detailSlice;
