import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { getMovies } from "../../services/movieAPI";

const moviesSlice = createSlice({
  name: "movieList",
  initialState: {
    status: "idle",
    movies: [
      { id: 0, title: "", poster_path: "", release_date: "", vote_average: 0 },
    ],
    loading: false,
  },
  reducers: {
    reset: (state) => {
      state.movies = [];
    },
  },

  extraReducers: (builder) => {
    builder
      .addCase(fetchMovies.pending, (state) => {
        state.status = "loading";
        state.loading = true;
      })
      .addCase(fetchMovies.fulfilled, (state, action) => {
        state.movies.push(...action.payload.results);
        state.status = "idle";
        state.loading = false;
      })
      .addCase(fetchMovies.rejected, (state) => {
        state.status = "failed";
        state.loading = false;
      });
  },
});

export const fetchMovies = createAsyncThunk(
  "movies/fetchMovies",
  async (page: number) => {
    const res = await getMovies(page);
    return res;
  }
);

export default moviesSlice;
