const API_URL = process.env.NEXT_PUBLIC_DB_URL;
const API_KEY = process.env.NEXT_PUBLIC_DB_KEY;
const BACKDROP_URL =
  "https://www.themoviedb.org/t/p/w1920_and_h800_multi_faces";
const POSTER_URL = "https://image.tmdb.org/t/p/w220_and_h330_face";
const BACKDROP_FILE_URL = "https://www.themoviedb.org/t/p/w533_and_h300_bestv2";
const LOGO_URL =
  "https://www.themoviedb.org/t/p/original/z0h7mBHwm5KfMB2MKeoQDD2ngEZ.jpg";
const PROFILE_URL = "https://www.themoviedb.org/t/p/w138_and_h175_face";
const AVATAR_URL = "https://www.themoviedb.org/t/p/w64_and_h64_face";
const RECOMMENDATION_URL = "https://www.themoviedb.org/t/p/w250_and_h141_face";
const TEASER_URL = "https://www.themoviedb.org/video/play?key=";
const YOUTUBE_URL = "https://www.youtube.com/embed/";
const POSTER_EXPAND = "https://image.tmdb.org/t/p/w600_and_h900_bestv2/";
const POSTER_IMAGE = "https://www.themoviedb.org/t/p/w220_and_h330_face";
export {
  API_URL,
  API_KEY,
  BACKDROP_URL,
  POSTER_URL,
  LOGO_URL,
  PROFILE_URL,
  AVATAR_URL,
  RECOMMENDATION_URL,
  TEASER_URL,
  BACKDROP_FILE_URL,
  YOUTUBE_URL,
  POSTER_EXPAND,
  POSTER_IMAGE,
};
