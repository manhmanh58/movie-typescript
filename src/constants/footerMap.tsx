export const LEGAL = [
  {
    label: "Terms of Uses",
  },
  {
    label: "API Terms of",
  },
  {
    label: "Use",
  },
  {
    label: "Privacy Policy",
  },
];

export const COMMUNITY = [
  {
    label: "Guidelines",
  },
  {
    label: "Discussions",
  },
  {
    label: "Leaderboard",
  },
  {
    label: "Twitter",
  },
];

export const INVOLVED = [
  {
    label: "Contribution Bible",
  },
  {
    label: "API Terms of",
  },
  {
    label: "Add New TV Show",
  },
];

export const Basics = [
  {
    label: "About TMDB",
  },
  {
    label: "Contact Us",
  },
  {
    label: "Support Forums",
  },
  {
    label: "API",
  },
  {
    label: "System Status",
  },
];
