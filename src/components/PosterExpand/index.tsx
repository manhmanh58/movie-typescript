import React from "react";
import styles from "./style.module.scss";
import { GrFormClose } from "react-icons/gr";
import { AiFillLock } from "react-icons/ai";
import { RiCloseCircleFill } from "react-icons/ri";
import { FaPlus } from "react-icons/fa";
import { HiOutlineArrowRight } from "react-icons/hi";
import { POSTER_EXPAND } from "@/src/constants/common";

interface InterfaceProps {
  poster_path: string;
  onToggleModal: Function;
}
const PosterExpand = ({ poster_path, onToggleModal }: InterfaceProps) => {
  return (
    <div id={styles.lightbox_popup}>
      <a href="/" className={styles.wrapper}>
        <img
          alt="poster"
          loading="lazy"
          width={390}
          height={585}
          src={`${POSTER_EXPAND}${poster_path}`}
        />
      </a>
      <div className={styles[`infor-poster`]}>
        <h3>
          <div className={styles[`close_right`]} onClick={() => onToggleModal}>
            <GrFormClose size={24} />
          </div>
        </h3>
        <div className={styles[`meta-wrapper`]}>
          <h3>
            Info <AiFillLock size={20} />
          </h3>
          <form className={styles[`style-form`]}>
            <label className={styles[`flex`]}>
              Primary? &nbsp;
              <RiCloseCircleFill size={20} />
            </label>
            <label>Added By</label>
            <p>
              <a href="/">JoeSSS</a>
            </p>
            <label>Size</label>
            <p>
              <a href="/">2000x3000</a>
            </p>
            <label>
              <span className={styles.flex}>Language</span>
              <div className={styles[`select-dropdown`]}>
                <select disabled>
                  <option value="en">English</option>
                </select>
              </div>
            </label>
          </form>
          <div className={styles[`tagged-people-wrapper`]}>
            <h3>
              Tagged People
              <a href="/">
                <FaPlus size={20} />
              </a>
            </h3>
            <ul className={styles[`tagged_people`]}>
              <li>No records have been added</li>
            </ul>
          </div>
          <div id={styles.paging}>
            <a className={styles.next} href="/">
              <HiOutlineArrowRight size={20} />
            </a>
          </div>
        </div>
      </div>
    </div>
  );
};
export default PosterExpand;
