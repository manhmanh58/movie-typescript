import styles from "./style.module.scss";
import Movies from "./components/MoviesList";
import Filter from "./components/Filter";
import Header from "../Header";
import Footer from "../Footer";
const MovieList = () => {
  return (
    <div>
      <Header />
      <div className={styles.content}>
        <Filter />
        <Movies />
      </div>
      <Footer />
    </div>
  );
};

export default MovieList;
