import React, { useState, useRef, useCallback, useEffect } from "react";
import styles from "./style.module.scss";
import MovieCard from "../MovieCard";
import moviesSlice, { fetchMovies } from "redux/movies/moviesSlice";
import { useAppSelector } from "hooks";
import { useDispatch } from "react-redux";
import { MovielistInterface } from "util/types/movieListType";

interface MoviesInterface {
  movies: Array<MovielistInterface>;
  loading: boolean;
}
const Movies = () => {
  const dispatch = useDispatch<any>();
  const { movies, loading }: MoviesInterface = useAppSelector(
    (state) => state.movieList
  );
  const [loadMore, setLoadMore] = useState(false);
  const [pageNum, setPageNum] = useState(1);
  const [progress, setProgress] = useState(0);
  const [opacity, setOpacity] = useState(1);
  const [visibility, setVisibility] = useState(true);
  const moviesObserver = useRef<IntersectionObserver | null>(null);

  useEffect(() => {
    if (pageNum === 1) {
      dispatch(moviesSlice.actions.reset());
    }
    dispatch(fetchMovies(pageNum));
  }, [pageNum]);

  const lastMovieElementRef = useCallback(
    (node: any) => {
      if (loading) return;
      if (moviesObserver.current) moviesObserver.current.disconnect();
      moviesObserver.current = new IntersectionObserver((entries) => {
        if (entries[0].isIntersecting && loadMore) {
          setProgress(100);
          setOpacity(1);
          setTimeout(() => {
            setPageNum(pageNum + 1);
            setOpacity(0);
            setProgress(0);
          }, 500);
        }
      });
      if (node) moviesObserver.current.observe(node);
    },
    [loading, loadMore, pageNum]
  );
  const handleLoadMore = () => {
    setVisibility(false);
    setLoadMore(true);
    setProgress(100);
    setTimeout(() => {
      setProgress(0);
      setOpacity(0);
      setPageNum((prevPage) => prevPage + 1);
    }, 500);
  };

  const content = movies.map((movie, index) => {
    if (movies.length === index + 1) {
      return (
        <MovieCard
          key={index}
          id={movie.id}
          ref={lastMovieElementRef}
          title={movie.title}
          poster_path={movie.poster_path}
          release_date={movie.release_date}
          vote_average={movie.vote_average}
        />
      );
    } else {
      return (
        <MovieCard
          key={index}
          id={movie.id}
          title={movie.title}
          poster_path={movie.poster_path}
          release_date={movie.release_date}
          vote_average={movie.vote_average}
        />
      );
    }
  });
  return (
    <div className={styles.page_wrapper}>
      <div className={styles.progress_box}>
        <div
          className={styles.loading_bar}
          style={{ width: `${progress}%`, opacity: `${opacity}` }}
        ></div>
      </div>
      {content}
      <div
        onClick={handleLoadMore}
        className={styles.load_btn}
        style={{ ...(!visibility ? { visibility: "hidden" } : {}) }}
      >
        <p className={styles.load_more}>Load More</p>
      </div>
    </div>
  );
};

export default Movies;
