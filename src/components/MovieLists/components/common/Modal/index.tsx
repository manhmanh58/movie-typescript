import React, { useRef } from "react";
import useOnClickOutside from "@/src/hooks/useOnClickOutside";
import styles from "./style.module.scss";

interface InterfaceProps {
  setModalOpen: Function;
}
export const Modal = ({ setModalOpen }: InterfaceProps) => {
  const ref: any = useRef();
  useOnClickOutside(ref, () => {
    setModalOpen(false);
  });
  return (
    <div ref={ref} className={styles.hover}>
      <div className={styles.modal_container}>
        <div className={styles.modal}>
          <div className={styles.modal_content}>
            <div className={styles.group}>
              <p className={styles.no_hover}>
                Want to rate or add this item to a list?
              </p>
              <p className={styles.hover}>
                <a href="/">Login</a>
              </p>
            </div>
            <div className={`${styles.group} ${styles.group_primary}`}>
              <p className={styles.no_hover}>Not a member?</p>
              <p className={styles.hover}>
                <a href="/">Sign up and join the community </a>
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
