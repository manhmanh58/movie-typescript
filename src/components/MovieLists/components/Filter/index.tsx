import React from "react";
import styles from "./Filter.module.scss";

const Filter = () => {
  return (
    <div className={styles.filter}>
      <div className={styles.filter__title}>
        <h2>Popular Movie</h2>
      </div>
      <div className={styles.filter__panel}>
        <div className={styles.filter__name}>
          <h2 className={styles.title}>Sort</h2>
          <span className={styles.chevron_right}></span>
        </div>
      </div>
      <div className={styles.filter__panel}>
        <div className={styles.filter__name}>
          <h2 className={styles.title}>Filters</h2>
          <span className={styles.chevron_right}></span>
        </div>
      </div>
      <div className={styles.filter__panel}>
        <div className={styles.filter__name}>
          <h2 className={styles.title}>Where To Watch</h2>
          <span className={styles.chevron_right}></span>
        </div>
      </div>
      <div className={styles.filter__search}>
        <p>
          <a className={styles[`filter__load-more`]} href="/">
            Search
          </a>
        </p>
      </div>
    </div>
  );
};

export default Filter;
