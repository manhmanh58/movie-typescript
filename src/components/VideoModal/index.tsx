import React from "react";
import styles from "./style.module.scss";
import { IoMdClose } from "react-icons/io";
import { YOUTUBE_URL } from "@/src/constants/common";
interface VideoInterface {
  isTrailerPlay: Boolean;
  setTrailerPlay: Function;
  trailer?: string;
}
const Video = ({ isTrailerPlay, setTrailerPlay, trailer }: VideoInterface) => {
  return (
    <div className={styles[`modal`]}>
      <div
        className={styles[`modal__overlay`]}
        style={{
          ...(!isTrailerPlay ? { display: "none" } : {}),
        }}
      ></div>
      <div className={styles[`modal__video`]}>
        <div className={styles[`title-bar`]}>
          <span className={styles[`title-bar__title`]}>Play Trailer</span>
          <div
            className={styles[`title-bar__close`]}
            onClick={() => setTrailerPlay(false)}
          >
            <IoMdClose style={{ fill: "white" }} size={16} />
          </div>
        </div>
        <div className={styles[`video`]}>
          <iframe
            width="100%"
            height="100%"
            title="Trailer"
            src={`${YOUTUBE_URL}${trailer}`}
            frameBorder="none"
          />
        </div>
      </div>
    </div>
  );
};

export default Video;
