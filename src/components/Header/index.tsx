import React, { useEffect, useState } from "react";
import styles from "./style.module.scss";
import Dropdown from "../Dropdown";
import { MOVIES, TV_SHOWS, PEOPLE, MORE } from "constants/header";
import themoviedb from "assets/images/Logo.svg";
import Image from "next/image";
const Header = () => {
  const [position, setPosition] = useState(
    typeof window !== "undefined" && window.pageYOffset
  );
  const [visible, setVisible] = useState(true);
  useEffect(() => {
    const handleScroll = () => {
      if (window.pageYOffset > 80) {
        let moving = window.pageYOffset;
        setVisible(position > moving);
        setPosition(moving);
      }
    };
    window.addEventListener("scroll", handleScroll);
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  });
  const cls = visible ? "visible" : "hidden";
  return (
    <header className={`${cls} ${styles.header}`}>
      <div className={styles.header__nav}>
        <a className={styles.header__logo} href="/">
          <Image src={themoviedb} alt="The Movie Database (TMDB)" />
        </a>
        <ul className={styles[`header__navigation`]}>
          <li>
            <Dropdown labels={MOVIES}>
              <a href="/">Movies</a>
            </Dropdown>
          </li>
          <li>
            <Dropdown labels={TV_SHOWS}>
              <a href="/">TV Shows</a>
            </Dropdown>
          </li>
          <li>
            <Dropdown labels={PEOPLE}>
              <a href="/">People</a>
            </Dropdown>
          </li>
          <li>
            <Dropdown labels={MORE}>
              <a href="/">More</a>
            </Dropdown>
          </li>
        </ul>
      </div>
      <div className={styles[`header__function`]}>
        <ul className={styles[`header__list`]}>
          <li>
            <a href="/">
              <span className={styles[`header__plus-icon`]}></span>
            </a>
          </li>
          <li className={styles[`header__translate`]}>
            <div>EN</div>
          </li>
          <li>
            <a href="/">Login</a>
          </li>
          <li>
            <a href="/">Join TMDB</a>
          </li>
          <li className={styles[`search`]}>
            <a className={styles[`search__link`]} href="/">
              <span className={styles[`search__icon`]}></span>
            </a>
          </li>
        </ul>
      </div>
    </header>
  );
};

export default Header;
