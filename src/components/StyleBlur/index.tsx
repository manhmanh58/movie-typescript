import React from "react";
import styles from "./style.module.scss";

interface Blur {
  hideFade: string | null;
}
const StyleBlur = ({ hideFade }: Blur) => {
  return (
    <div
      className={styles.blur}
      style={{ ...(hideFade === "1" ? { opacity: 1 } : { opacity: 0 }) }}
    ></div>
  );
};

export default StyleBlur;
