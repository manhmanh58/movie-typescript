import React, { useState } from "react";
import styles from "./style.module.scss";
import { VisitInterface } from "util/types/componentsType";

const Tooltip = ({ children, message }: VisitInterface) => {
  const [isOpen, setIsOpen] = useState(false);

  return (
    <li
      className={styles[`tooltip`]}
      onMouseEnter={() => setIsOpen(true)}
      onMouseLeave={() => setIsOpen(false)}
    >
      <div className={styles[`tooltip-box`]}>
        {children}
        {isOpen && (
          <div className={styles[`tooltip-box__popup`]}>{message}</div>
        )}
      </div>
    </li>
  );
};

export default Tooltip;
