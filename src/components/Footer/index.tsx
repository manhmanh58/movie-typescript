import React from "react";
import styles from "./style.module.scss";
import footer from "assets/images/Logofooter.svg";
import { Basics, INVOLVED, COMMUNITY, LEGAL } from "constants/footerMap";
import Image from "next/image";
const Footer = () => {
  return (
    <footer className={styles.footer}>
      <nav className={styles.nav}>
        <div className={styles[`join`]}>
          <div
            style={{
              width: "130px",
              height: "94px",
              position: "absolute",
              top: "0",
              right: "0",
              marginRight: "0",
            }}
          >
            <Image src={footer} alt="The Movie Database (TMDB)" layout="fill" />
          </div>

          <a className={styles[`join__logo`]} href="/">
            Join the Community
          </a>
        </div>
        <div>
          <h3>The Basics</h3>
          <ul>
            {Basics.map((basic, index) => (
              <li key={index}>
                <a href="/">{basic.label}</a>
              </li>
            ))}
          </ul>
        </div>
        <div>
          <h3>Get Involved</h3>
          <ul>
            {INVOLVED.map((involve, index) => (
              <li key={index}>
                <a href="/">{involve.label}</a>
              </li>
            ))}
          </ul>
        </div>
        <div>
          <h3>Community</h3>
          <ul>
            {COMMUNITY.map((Community, index) => (
              <li key={index}>
                <a href="/">{Community.label}</a>
              </li>
            ))}
          </ul>
        </div>
        <div>
          <h3>Legal</h3>
          <ul>
            {LEGAL.map((legal, index) => (
              <li key={index}>
                <a href="/">{legal.label}</a>
              </li>
            ))}
          </ul>
        </div>
      </nav>
    </footer>
  );
};

export default Footer;
