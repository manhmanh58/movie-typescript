import React, { useState } from "react";
import styles from "./style.module.scss";
import { VisitInterface } from "util/types/componentsType";

const Visit = ({ children, message }: VisitInterface) => {
  const [isOpen, setIsOpen] = useState(false);
  return (
    <div className={styles[`visit`]}>
      <a
        href="/"
        className={styles[`visit__link`]}
        onMouseEnter={() => setIsOpen(true)}
        onMouseLeave={() => setIsOpen(false)}
      >
        {isOpen && <div className={styles[`visit__popup`]}>{message}</div>}
        {children}
      </a>
    </div>
  );
};

export default Visit;
