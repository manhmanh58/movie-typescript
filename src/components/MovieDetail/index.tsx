import React, { useEffect } from "react";
import styles from "./style.module.scss";
import Header from "@/src/components/Header";
import ShortcutBar from "./components/ShortcutBar/index";
import Footer from "@/src/components/Footer";
import {
  fetchDetail,
  fetchProfile,
  fetchReview,
  fetchRecommendation,
  fetchKeyword,
  fetchPoster,
} from "redux/detail/detailSlice";
import { useRouter } from "next/router";
import Media from "./components/Media";
import { useDispatch } from "react-redux";
import HeaderBackground from "./components/HeaderBackground";

const MovieDetail = () => {
  const dispatch = useDispatch<any>();
  const router = useRouter();
  const id = router.query.id as string;

  useEffect(() => {
    if (id) {
      dispatch(fetchReview(id));
      dispatch(fetchDetail(id));
      dispatch(fetchProfile(id));
      dispatch(fetchRecommendation(id));
      dispatch(fetchKeyword(id));
      dispatch(fetchPoster(id));
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id]);
  return (
    <div>
      <Header />
      <div className={styles.main}>
        <ShortcutBar />
        <HeaderBackground />
        <Media />
      </div>
      <Footer />
    </div>
  );
};

export default MovieDetail;
