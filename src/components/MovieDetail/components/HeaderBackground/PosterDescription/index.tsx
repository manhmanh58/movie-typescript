import React from "react";
import styles from "./style.module.scss";
import { CircularProgressbarWithChildren } from "react-circular-progressbar";
import { AiOutlinePercentage } from "react-icons/ai";
import dateFormat from "dateformat";
import Tooltip from "@/src/components/TooltipModal";
import { chain } from "lodash";
import { useSelector } from "react-redux";

interface DetailInterface {
  title: string;
  release_date: string;
  genres: Array<{}>;
  runtime: number;
  tagline: string;
  overview: string;
}
interface Descriptions {
  detail: DetailInterface;
  setTrailerPlay: Function;
  percentage: number;
}
const Description = (props: Descriptions) => {
  const crewApi = useSelector((state: any) => state.detail.profile.crew);
  const crew = chain(crewApi)
    .filter((obj) => {
      const jobs = ["Characters", "Screenplay", "Director", "Story", "Writer"];
      return jobs.includes(obj.job);
    })
    .orderBy(["popularity"], ["desc"])
    .groupBy("name")
    .transform(function (result: any, val: any, key: string) {
      result.push({ [key]: val.map((i: { job: string }) => i.job).join(", ") });
    }, [])
    .value();

  const handleClick = () => {
    props.setTrailerPlay(true);
  };
  return (
    <div className={styles[`header-poster`]}>
      <div className={styles[`header-poster__title`]}>
        <h2>
          <a href="/">{props.detail.title}</a>
          <span className={styles[`header-poster__release-date`]}>
            ({dateFormat(props.detail.release_date, "yyyy")})
          </span>
        </h2>
        <div className={styles[`header-poster__facts`]}>
          <span className={styles[`header-poster__certification`]}>PG-13</span>
          <span className={styles[`header-poster__release`]}>
            {dateFormat(props.detail.release_date, "mm/dd/yyyy")} (US)
          </span>
          <span className={styles[`header-poster__genres`]}>
            {props.detail.genres?.map((genre: any, index) => (
              <a key={index} href="/">
                {genre.name}
              </a>
            ))}
          </span>
          <span className={styles[`header-poster__runtime`]}>{`${Math.floor(
            props.detail.runtime / 60
          )}h ${props.detail.runtime % 60}m`}</span>
        </div>
      </div>
      <ul className={styles.action}>
        <li className={styles.chart}>
          <div className={styles.details}>
            <div className={styles.outer_ring}>
              <div className={styles.user_score}>
                <CircularProgressbarWithChildren
                  value={props.percentage}
                  styles={{
                    // Customize the path
                    path: {
                      stroke:
                        props.percentage >= 70
                          ? `rgba(33, 208, 122, ${props.percentage / 100})`
                          : `rgba(210,213,49, ${props.percentage / 100})`,

                      strokeLinecap: "round",
                    },
                    // Customize the circle behind the path
                    trail: {
                      stroke: props.percentage >= 70 ? "#204529" : "#423d0f",
                      strokeLinecap: "round",
                    },
                  }}
                >
                  <div
                    style={{
                      marginBottom: "15px",
                    }}
                  >
                    <strong
                      style={{
                        fontSize: 23,
                        marginTop: -40,
                        color: "#f2f3f4",
                      }}
                    >{`${props.percentage}`}</strong>
                  </div>
                </CircularProgressbarWithChildren>
                <div className={styles.percent}>
                  <AiOutlinePercentage size={8} style={{ fill: "white" }} />
                </div>
              </div>
            </div>
          </div>
          <div className={styles.text}>
            User
            <br />
            Score
          </div>
        </li>
        <Tooltip message="Login to create and edit custom lists">
          <span className={styles.tooltip__list}></span>
        </Tooltip>
        <Tooltip message="Login to create and edit custom lists">
          <span className={styles.tooltip__heart}></span>
        </Tooltip>
        <Tooltip message="Login to create and edit custom lists">
          <span className={styles.tooltip__bookmark}></span>
        </Tooltip>
        <Tooltip message="Login to rate this movie">
          <span className={styles.tooltip__rating}></span>
        </Tooltip>
        <li className={styles.video}>
          <p className={styles.video__content} onClick={handleClick}>
            <span className={styles.video__play}> </span>Play Trailer
          </p>
        </li>
      </ul>
      <div className={styles[`header-info`]}>
        <h3 className={styles[`header-info__tagline`]}>
          {props.detail.tagline}
        </h3>
        <h3>Overview</h3>
        <div className={styles[`header-info__overview`]}>
          <p>{props.detail.overview}</p>
        </div>
        <ol className={styles[`header-info__people`]}>
          {crew.slice(0, 5).map((item: any, index: number) => {
            const key = Object.keys(item)[0];
            return (
              <li className={styles[`header-info__profile`]} key={index}>
                <p>
                  <a href="/">{key}</a>
                </p>
                <p className={styles[`header-info__character`]}>{item[key]}</p>
              </li>
            );
          })}
        </ol>
      </div>
    </div>
  );
};

export default Description;
