import React, { useState, useEffect } from "react";
import styles from "./style.module.scss";
import Video from "@/src/components/VideoModal";
import { fetchTrailer } from "redux/detail/detailSlice";
import { useRouter } from "next/router";
import { BACKDROP_URL } from "@/src/constants/common";
import Poster from "./Poster";
import Description from "./PosterDescription";
import { useAppSelector } from "hooks";
import { useDispatch } from "react-redux";
import { TrailerInterface } from "util/types/componentsType";

interface InterfaceProps {
  detail: {
    vote_average: number;
    backdrop_path: string;
    poster_path: string;
    title: string;
    release_date: string;
    genres: Array<{}>;
    runtime: number;
    tagline: string;
    overview: string;
  };
  trailer: Array<TrailerInterface>;
}
const HeaderBackground = () => {
  const { detail, trailer }: InterfaceProps = useAppSelector(
    (state) => state.detail
  );
  const router = useRouter();
  const id = router.query.id as string;
  const trailers = trailer?.find((c: any) => c.type === "Trailer");
  const [isModalPosterOpen, setModalPosterOpen] = useState(false);
  const [isTrailerPlay, setTrailerPlay] = useState(false);
  const percentage = Math.round(detail.vote_average * 10);
  const dispatch = useDispatch<any>();

  useEffect(() => {
    dispatch(fetchTrailer(id));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div
      className={styles[`header-lagre`]}
      style={{
        backgroundImage: `url(${BACKDROP_URL}${detail.backdrop_path})`,
      }}
    >
      <div className={styles[`header-lagre__custom-bg`]}>
        <Poster
          poster_path={detail.poster_path}
          isModalPosterOpen={isModalPosterOpen}
          setModalPosterOpen={setModalPosterOpen}
        />
        <Description
          detail={detail}
          percentage={percentage}
          setTrailerPlay={setTrailerPlay}
        />
      </div>
      {isTrailerPlay && (
        <Video
          trailer={trailers?.key}
          isTrailerPlay={isTrailerPlay}
          setTrailerPlay={setTrailerPlay}
        />
      )}
    </div>
  );
};

export default HeaderBackground;
