import React, { useState } from "react";
import styles from "./style.module.scss";
import { POSTER_URL, LOGO_URL } from "@/src/constants/common";
import PosterExpand from "@/src/components/PosterExpand";

interface Posters {
  poster_path: string;
  setModalPosterOpen: Function;
  isModalPosterOpen: boolean;
}
const Poster = (props: Posters) => {
  const [isOpen, setIsOpen] = useState(false);
  function toggleModal() {
    setIsOpen(!isOpen);
  }

  return (
    <>
      <div className={styles[`poster`]}>
        <div
          className={styles[`poster__image`]}
          onMouseEnter={() => props.setModalPosterOpen(true)}
          onMouseLeave={() => props.setModalPosterOpen(false)}
        >
          <img alt="Movie x" src={`${POSTER_URL}${props.poster_path}`} />

          <div
            onClick={toggleModal}
            className={styles[`zoom`]}
            style={{
              ...(props.isModalPosterOpen ? { opacity: "1" } : {}),
            }}
          >
            <div className={styles[`zoom__no-click`]} onClick={toggleModal}>
              <span className={styles[`zoom__icon`]}></span> <p>Expand</p>
            </div>
          </div>
        </div>

        <div className={styles[`offer`]}>
          <div className={styles[`offer__text-wrapper`]}>
            <div className={styles[`offer__button`]}>
              <div className={styles[`offer__provider`]}>
                <img
                  src={`${LOGO_URL}`}
                  width="36"
                  height="36"
                  alt="Roku chanel"
                />
              </div>
              <div className={styles[`offer__text`]}>
                <span>
                  <h4>Now Streaming</h4>
                  <h3>Watch Now</h3>
                </span>
              </div>
            </div>
          </div>
        </div>
        {isOpen && (
          <div className={styles.modal}>
            <div className={styles.modal__overlay}></div>
            <div className={styles.modal__style}>
              <PosterExpand
                poster_path={props.poster_path}
                onToggleModal={toggleModal}
              />
            </div>
          </div>
        )}
      </div>
    </>
  );
};

export default Poster;
