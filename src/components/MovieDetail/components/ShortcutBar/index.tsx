import React from "react";
import styles from "./styles.module.scss";
import { OVERVIEW, SHARE, MEDIA, FANDOM } from "@/src/constants/shortcutBar";
import { GoTriangleDown } from "react-icons/go";
import Dropdown from "@/src/components/Dropdown";
const ShortcutBar = () => {
  return (
    <div className={styles[`shortcut-bar`]}>
      <ul className={styles[`shortcut-bar__list`]}>
        <li className={`${styles[`shortcut-bar__item`]} ${styles.true}`}>
          <Dropdown labels={OVERVIEW}>
            <span className={styles[`shortcut-bar__link`]}>
              Overview
              <GoTriangleDown className={styles[`shortcut-bar__icon`]} />
            </span>
          </Dropdown>
        </li>
        <li className={`${styles[`shortcut-bar__item`]}`}>
          <Dropdown labels={MEDIA}>
            <span className={styles[`shortcut-bar__link`]}>
              Media
              <GoTriangleDown className={styles[`shortcut-bar__icon`]} />
            </span>
          </Dropdown>
        </li>
        <li className={`${styles[`shortcut-bar__item`]}`}>
          <Dropdown labels={FANDOM}>
            <span className={styles[`shortcut-bar__link`]}>
              Fandom
              <GoTriangleDown className={styles[`shortcut-bar__icon`]} />
            </span>
          </Dropdown>
        </li>
        <li className={`${styles[`shortcut-bar__item`]}`}>
          <Dropdown labels={SHARE}>
            <span className={styles[`shortcut-bar__link`]}>
              Share
              <GoTriangleDown className={styles[`shortcut-bar__icon`]} />
            </span>
          </Dropdown>
        </li>
      </ul>
    </div>
  );
};

export default ShortcutBar;
