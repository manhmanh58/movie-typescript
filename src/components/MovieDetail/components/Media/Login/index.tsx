import React from "react";
import styles from "./style.module.scss";

const Login = () => {
  return (
    <div className={styles.login}>
      <p className={styles.login__button}>
        <a href="/">
          <span className={styles.login__lock}></span> Login to edit
        </a>
      </p>
      <div className={styles.keyboard}>
        <a href="/" className={styles.keyboard__shortcuts}>
          <span className={styles.keyboard__icon}></span> Keyboard Shortcuts
        </a>
      </div>
      <div className={styles.issues}>
        <a href="/" className={styles.issues__shortcuts}>
          <span className={styles.issues__icon}></span> Login to report an issue
        </a>
      </div>
    </div>
  );
};

export default Login;
