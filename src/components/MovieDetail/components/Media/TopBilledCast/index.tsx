import React, { useState } from "react";
import styles from "./styles.module.scss";
import { useAppSelector } from "hooks";
import { PROFILE_URL } from "@/src/constants/common";
import StyleBlur from "@/src/components/StyleBlur";

interface InterfaceProps {
  profile_path: string;
  character: string;
  name: string;
}
const TopBilledCast = () => {
  const profileApi = useAppSelector<Array<InterfaceProps>>(
    (state) => state.detail.profile.cast
  );
  const [scrollPosition, setScrollPosition] = useState(0);
  const onPeopleScroll = (e: Event) => {
    const position = (e.target as HTMLDivElement).scrollLeft;
    setScrollPosition(position);
  };

  return (
    <div className={styles[`top-billed`]}>
      <h3 className={styles[`top-billed__title`]}>Top Billed Cast</h3>
      <div className={styles[`top-billed__cast`]}>
        <ol
          className={styles[`top-billed__people`]}
          onScroll={() => onPeopleScroll}
        >
          {profileApi?.slice(0, 8).map((profile, index) => (
            <li className={styles[`top-billed__card`]} key={index}>
              <a className={styles[`top-billed__image`]} href="/">
                {profile.profile_path !== null ? (
                  <img
                    alt="card"
                    src={`${PROFILE_URL}${profile.profile_path}`}
                  />
                ) : (
                  <div className={styles[`top-billed__picture`]}> </div>
                )}
              </a>
              <p>
                <a href="/">{`${profile.name}`}</a>
              </p>
              <p
                className={styles[`top-billed__character`]}
              >{`${profile.character}`}</p>
            </li>
          ))}
          <li className={styles[`top-billed__filler`]}>
            <p>
              View More
              <span className={styles[`top-billed__array-icon`]}></span>
            </p>
          </li>
        </ol>
        <StyleBlur hideFade={scrollPosition > 100 ? null : "1"} />
      </div>
      <p className={styles[`top-billed__button`]}>
        <a href="/">Full Cast &amp; Crew</a>
      </p>
    </div>
  );
};

export default TopBilledCast;
