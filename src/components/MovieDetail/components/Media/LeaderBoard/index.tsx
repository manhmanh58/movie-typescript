import React from "react";
import styles from "./style.module.scss";
import { CONTRIBUTOR } from "@/src/constants/movieDetailMap";

const LeaderBoard = () => {
  return (
    <div className={styles[`leader-board`]}>
      <h4>Top Contributors</h4>
      {CONTRIBUTOR.map((contribu, index) => (
        <div key={index} className={styles[`leader-board__edit`]}>
          <div className={styles[`leader-board__avatar`]}>
            <a href="/">
              <img
                alt="avatar"
                className={styles[`leader-board__image`]}
                src={contribu.avatar}
              />
            </a>
          </div>
          <div className={styles[`leader-board__info`]}>
            <p className={styles[`leader-board__edit-count`]}>
              {contribu.score}
              <br />
              <a href="/">{contribu.name}</a>
            </p>
          </div>
        </div>
      ))}
      <a href="/" className={styles[`leader-board__edit`]}>
        View Edit History
      </a>
    </div>
  );
};

export default LeaderBoard;
