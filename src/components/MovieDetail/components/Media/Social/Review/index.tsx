import React from "react";
import styles from "./style.module.scss";
import { useAppSelector } from "hooks";

interface AuterdetailInterface {
  avatar_path: string;
  author: string;
  rating: number;
}
interface ReviewsInterface {
  author_details: AuterdetailInterface;
  author: string;
  content: string;
}

const Review = () => {
  const reviewApi = useAppSelector<Array<ReviewsInterface>>(
    (state) => state.detail.review
  );
  return (
    <div>
      <div className={styles[`media-panel__content`]}>
        <div className={styles[`media-panel__grouped`]}>
          <div className={styles[`media-panel__avatar`]}>
            <img
              alt="avatar"
              src={`${reviewApi[0]?.author_details?.avatar_path?.slice(1)}`}
            />
          </div>
          <div className={styles[`media-panel__info`]}>
            <div className={styles[`media-panel__rating`]}>
              <h3>
                <a href="/">{`A review by ${reviewApi[0]?.author}`}</a>
              </h3>
              <div className={styles[`media-panel__star`]}>
                <span className={styles[`media-panel__icon`]}></span>
                {reviewApi[0]?.author_details?.rating}
              </div>
            </div>
            <h5>
              Written by <a href="/">{reviewApi[0]?.author}</a> on September 12,
              2022
            </h5>
          </div>
        </div>
        <div className={styles[`media-panel__teaser`]}>
          <p className={styles[`media-panel__spoil`]}>
            <strong>
              Fall is 47 Meters Down but with heights instead of depths - 47
              Meter Up.
            </strong>
          </p>
          <p className={styles.text}>{reviewApi[0]?.content}</p>
          <a className={styles[`media-panel__link--underline`]} href="/">
            read the rest.
          </a>
        </div>
      </div>
      <p className={styles[`media-panel__button`]}>
        <a href="/">Read All Reviews</a>
      </p>
    </div>
  );
};

export default Review;
