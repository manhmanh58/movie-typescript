import React, { useState } from "react";
import styles from "./styles.module.scss";
import Review from "./Review";
import Discussions from "./Discussion";
import { useAppSelector } from "hooks";

interface Reviews {
  total_results: number;
}
const Social = () => {
  const [isReviewOpen, setIsReviewOpen] = useState(true);
  const reviewApi = useAppSelector<Reviews>(
    (state: { detail: { review: any } }) => state.detail.review
  );
  return (
    <div className={styles[`media-panel`]}>
      <div className={styles[`media-panel__menu`]}>
        <h3>Social</h3>
        <ul>
          <li
            className={styles[`media-panel__active`]}
            onClick={() => setIsReviewOpen(true)}
            style={{
              ...(isReviewOpen ? { borderBottom: "4px solid #000000" } : {}),
            }}
          >
            Reviews <span>{reviewApi.total_results}</span>
          </li>
          <li
            style={{
              ...(!isReviewOpen ? { borderBottom: "4px solid #000000" } : {}),
            }}
            onClick={() => setIsReviewOpen(false)}
          >
            Discussions <span>8</span>
          </li>
        </ul>
      </div>
      {isReviewOpen ? <Review /> : <Discussions />}
    </div>
  );
};

export default Social;
