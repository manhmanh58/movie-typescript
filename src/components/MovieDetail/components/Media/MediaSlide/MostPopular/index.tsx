import React, { useState } from "react";
import styles from "./style.module.scss";
import Video from "@/src/components/VideoModal";
import { BACKDROP_URL, POSTER_URL } from "@/src/constants/common";
import { TrailerInterface, DetailInterface } from "util/types/componentsType";

interface InterfaceProps {
  trailer: Array<TrailerInterface>;
  detail: DetailInterface;
}
const MostPopular = (props: InterfaceProps) => {
  const trailerKey = props.trailer?.find((i) => i.type === "Trailer");
  const [isTrailerPlay, setTrailerPlay] = useState(false);
  return (
    <>
      <div
        className={styles[`most-popular__video`]}
        style={{
          backgroundImage: `url(${`https://i.ytimg.com/vi/${trailerKey?.key}/hqdefault.jpg`})`,
        }}
        onClick={() => setTrailerPlay(true)}
      >
        <div className={styles[`most-popular__trailer`]}>
          <span className={styles[`most-popular__play-trailer`]}></span>
        </div>
      </div>
      <div className={styles[`most-popular__backdrop`]}>
        <img
          alt="backdrop"
          src={`${BACKDROP_URL}/${props.detail.backdrop_path}`}
        />
      </div>
      <div className={styles[`most-popular__poster`]}>
        <img alt="poster" src={`${POSTER_URL}/${props.detail.poster_path}`} />
      </div>
      {isTrailerPlay && (
        <Video
          trailer={trailerKey?.key}
          isTrailerPlay={isTrailerPlay}
          setTrailerPlay={setTrailerPlay}
        />
      )}
    </>
  );
};

export default MostPopular;
