import React from "react";
import styles from "./style.module.scss";
import { POSTER_IMAGE } from "@/src/constants/common";
interface InterfaceProps {
  posters: Array<{
    file_path: string;
  }>;
}
const Posters = (props: InterfaceProps) => {
  return (
    <>
      {props.posters.slice(0, 5).map((poster, index) => (
        <div className={styles[`posters__poster`]} key={index}>
          <img alt="poster" src={`${POSTER_IMAGE}${poster.file_path}`} />
        </div>
      ))}
      <div className={styles[`view-more`]}>
        <p>
          View More
          <span className={styles[`view-more__array-icon`]}></span>
        </p>
      </div>
    </>
  );
};

export default Posters;
