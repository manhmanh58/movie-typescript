import React, { useState } from "react";
import styles from "./style.module.scss";
import Video from "@/src/components/VideoModal";
import { TrailerInterface } from "util/types/componentsType";

interface InterfaceProps {
  trailer: TrailerInterface;
}
const VideoCard = (props: InterfaceProps) => {
  const [isTrailerPlay, setTrailerPlay] = useState(false);
  return (
    <>
      <div
        className={styles[`videos__video`]}
        style={{
          backgroundImage: `url(${`https://i.ytimg.com/vi/${props.trailer.key}/hqdefault.jpg`})`,
        }}
        onClick={() => setTrailerPlay(true)}
      >
        <div className={styles[`videos__trailer`]}>
          <span className={styles[`videos__play-trailer`]}></span>
        </div>
      </div>
      {isTrailerPlay && (
        <Video
          trailer={props.trailer?.key}
          isTrailerPlay={isTrailerPlay}
          setTrailerPlay={setTrailerPlay}
        />
      )}
    </>
  );
};

export default VideoCard;
