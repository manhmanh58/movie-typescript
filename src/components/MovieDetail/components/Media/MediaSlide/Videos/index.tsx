import React from "react";
import styles from "./style.module.scss";
import VideoCard from "./VideoCard";
import { TrailerInterface } from "util/types/componentsType";

interface InterfaceProps {
  trailers: Array<TrailerInterface>;
}
const Videos = (props: InterfaceProps) => {
  const videos = props.trailers.filter((values, index) => {
    if (
      values.type === "Trailer" ||
      values.type === "Teaser" ||
      values.type === "Clip"
    ) {
      return true;
    }
    return false;
  });

  return (
    <>
      {videos.slice(0, 5).map((trailer, index) => (
        <VideoCard trailer={trailer} key={index} />
      ))}
      <div className={styles[`view-more`]}>
        <p>
          View More
          <span className={styles[`view-more__array-icon`]}></span>
        </p>
      </div>
    </>
  );
};

export default Videos;
