import React from "react";
import styles from "./style.module.scss";
import { BACKDROP_FILE_URL } from "@/src/constants/common";

interface InterfaceProps {
  backdrops: Array<{ file_path: string }>;
}
const BackDrops = (props: InterfaceProps) => {
  return (
    <>
      {props.backdrops.slice(0, 6).map((backdrop, index: number) => (
        <div key={index} className={styles[`backdrops__backdrop`]}>
          <img
            alt="backdrop"
            src={`${BACKDROP_FILE_URL}${backdrop.file_path}`}
          />
        </div>
      ))}
      <div className={styles[`view-more`]}>
        <p>
          View More
          <span className={styles[`view-more__array-icon`]}></span>
        </p>
      </div>
    </>
  );
};

export default BackDrops;
