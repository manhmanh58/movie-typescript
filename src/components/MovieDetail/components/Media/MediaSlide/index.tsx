import React, { useState } from "react";
import styles from "./styles.module.scss";
import BackDrops from "./BackDrops";
import MostPopular from "./MostPopular";
import Posters from "./Posters";
import Videos from "./Videos";
import StyleBlur from "@/src/components/StyleBlur";
import { useAppSelector } from "hooks";
import { TrailerInterface, DetailInterface } from "util/types/componentsType";

interface ImageInterface {
  backdrops: Array<{ file_path: string }>;
  posters: Array<{ file_path: string }>;
}
interface MediaInterface {
  trailer: Array<TrailerInterface>;
  detail: DetailInterface;
  images: ImageInterface;
}
const MediaSlices = () => {
  const { trailer, detail, images } = useAppSelector<MediaInterface>(
    (state) => state.detail
  );
  const [selectedTab, setSelectedTab] = useState(0);
  const onClickTab = (id: number) => {
    setSelectedTab(id);
  };
  const [scrollPosition, setScrollPosition] = useState(0);
  const onPeopleScroll = (e: any) => {
    const position = e.target.scrollLeft;
    setScrollPosition(position);
  };
  const handleRender = (id: number) => {
    switch (id) {
      case 0:
        return <MostPopular trailer={trailer} detail={detail} />;
      case 1:
        return <Videos trailers={trailer} />;
      case 2:
        return <BackDrops backdrops={images.backdrops} />;
      case 3:
        return <Posters posters={images.posters} />;
      default:
        break;
    }
  };
  return (
    <div className={styles[`media-panel`]}>
      <div className={styles[`media-panel__menu`]}>
        <h3>Media</h3>
        <ul>
          <li
            className={styles[`media-panel__active`]}
            onClick={() => onClickTab(0)}
            style={{
              ...(selectedTab === 0
                ? { borderBottom: "4px solid #000000" }
                : {}),
            }}
          >
            Most Popular <span>6</span>
          </li>
          <li
            style={{
              ...(selectedTab === 1
                ? { borderBottom: "4px solid #000000" }
                : {}),
            }}
            onClick={() => onClickTab(1)}
          >
            Videos <span>{trailer?.length}</span>
          </li>
          <li
            style={{
              ...(selectedTab === 2
                ? { borderBottom: "4px solid #000000" }
                : {}),
            }}
            onClick={() => onClickTab(2)}
          >
            Backdrops <span>{images.backdrops?.length}</span>
          </li>
          <li
            style={{
              ...(selectedTab === 3
                ? { borderBottom: "4px solid #000000" }
                : {}),
            }}
            onClick={() => onClickTab(3)}
          >
            Posters <span>{images.posters?.length}</span>
          </li>
        </ul>
      </div>
      <div className={styles[`scroll-wrapper`]}>
        <div className={styles[`scroll`]} onScroll={onPeopleScroll}>
          {handleRender(selectedTab)}
        </div>
        <StyleBlur hideFade={scrollPosition > 100 ? null : "1"} />
      </div>
    </div>
  );
};

export default MediaSlices;
