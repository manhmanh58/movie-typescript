import React from "react";
import styles from "./style.module.scss";
import Image from "next/image";
import TrendImg from "assets/images/Trend.png";

const Trend = () => {
  return (
    <div className={styles.trend}>
      <h4 className={styles.trend__title}>Popularity Trend</h4>
      <div
        style={{
          position: "relative",
          left: "-6px",
          width: "200px",
          height: "50px",
        }}
      >
        <Image alt="Trend" src={TrendImg} layout="fill"></Image>
      </div>
    </div>
  );
};

export default Trend;
