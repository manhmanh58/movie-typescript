import React from "react";
import styles from "./style.module.scss";
import Contact from "./Contact";
import ContentScore from "./ContentScore";
import KeyWord from "./Keyword";
import LeaderBoard from "./LeaderBoard";
import Trend from "./Trend";
import Login from "./Login";
import TopBilledCast from "./TopBilledCast";
import Social from "./Social";
import MediaSlices from "./MediaSlide";
import Recommendations from "./Recommentdations";

const Media = () => {
  return (
    <div className={styles[`media`]}>
      <div className={styles[`left-column`]}>
        <TopBilledCast />
        <Social />
        <MediaSlices />
        <Recommendations />
      </div>
      <div className={styles[`right-column`]}>
        <Contact />
        <KeyWord />
        <ContentScore />
        <LeaderBoard />
        <Trend />
        <Login />
      </div>
    </div>
  );
};

export default Media;
