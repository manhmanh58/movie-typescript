import React from "react";
import styles from "./style.module.scss";
import Visit from "@/src/components/visitModal";
const Contact = () => {
  return (
    <div className={styles.contact__facts}>
      <div className={styles[`contact__social-links`]}>
        <Visit message="Visit Facebook">
          <span className={styles[`contact__social-facebook`]}> </span>
        </Visit>

        <Visit message="Visit Twitter">
          <span className={styles[`contact__social-twitter`]}> </span>
        </Visit>

        <Visit message="Visit Instagram">
          <span className={styles[`contact__social-instagram`]}> </span>
        </Visit>

        <Visit message="Visit Home">
          <span className={styles[`contact__social-home`]}> </span>
        </Visit>
      </div>
      <p className={styles.contact}>
        <strong>Status</strong>
        Released
      </p>
      <p className={styles.contact}>
        <strong>Original Language</strong>
        English
      </p>
      <p className={styles.contact}>
        <strong>Budget</strong>
        $3,000,000.00
      </p>
      <p className={styles.contact}>
        <strong>Revenue</strong>
        $11,900,000.00
      </p>
    </div>
  );
};

export default Contact;
